var gulp = require('gulp'); //то что в node_modules

   var less = require('gulp-less');
   var prefix = require('gulp-autoprefixer');
   var concat = require('gulp-concat');
   var clean = require('gulp-clean-css');

gulp.task('hello',function () {

    console.log('Hello,Gulp');
})

gulp.task('less',function () {
    return gulp.src('./develop/styles/*.less')
        .pipe(less())
        .pipe(prefix(
            {
                browsers:['last 4 versions'],
                cascade:false
            }
        ))
        .pipe(gulp.dest('./public/styles'));
});

gulp.task('build',function () {
    return gulp.src('./develop/styles/*.less')
        .pipe(less())
        .pipe(prefix(
            {
                browsers:['last 4 versions'],
                cascade:false
            }
        ))
        .pipe(concat('main.min.css'))
        .pipe(clean())
        .pipe(gulp.dest('./build'))

});